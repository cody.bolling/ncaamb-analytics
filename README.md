![Project Avatar](repo-icon.png)

# NCCAmb Analytics

Contains scripts for pulling and analyzing NCAA men's basketball data.

## Project Structure

```text
base-project-template
│ README.md
│ CHANGELOG.md
| .gitlab-ci.yml
| .gitignore
| repo-icon.png
```

## Roadmap
